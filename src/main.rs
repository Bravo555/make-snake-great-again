extern crate snake;

extern crate pancurses;

use snake::{GameSettings, Snake};
use std::thread;
use std::time::Duration;

fn main() {
    let mut game = GameSettings::new()
        .width(30)
        .height(30)
        .snake(Snake::new(4, 4))
        .build();

    game.run();
}

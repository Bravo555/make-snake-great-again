extern crate pancurses;

mod direction;
use direction::Direction;

mod renderer;
use renderer::Renderer;

mod snake;
pub use snake::Snake;

mod point;

type Map = Vec<Vec<Tile>>;

#[derive(Debug)]
pub struct Game {
    width: u8,
    height: u8,
    snakes: Vec<Snake>,
    map: Map,

    window: pancurses::Window,
    renderer: Renderer,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Tile {
    Empty,
    Wall,
    Apple,
    Snake(usize),
}

impl Game {
    pub fn run(&mut self) {
        loop {
            self.draw();
            self.handle_input();
            self.update();
        }
    }

    fn handle_input(&mut self) {
        use pancurses::Input::*;
        use std::time::{Duration, Instant};

        self.window.keypad(true);
        pancurses::noecho();
        self.window.nodelay(true);

        let now = Instant::now();
        while Instant::now() - now < Duration::from_millis(300) {
            if let Some(ch) = self.window.getch() {
                match ch {
                    KeyLeft | KeyRight | KeyUp | KeyDown => {
                        let dir = match ch {
                            KeyLeft => Direction::Left,
                            KeyRight => Direction::Right,
                            KeyUp => Direction::Up,
                            KeyDown => Direction::Down,
                            _ => Direction::Left,
                        };
                        if self.snakes[0].direction.reverse() != dir {
                            self.snakes[0].direction = dir;
                        }
                    }
                    KeyResize => {
                        pancurses::resize_term(0, 0);
                        self.draw();
                    }
                    a => {
                        println!("{:?}", a);
                    }
                }
            }
        }
    }

    fn draw(&self) {
        self.renderer.draw(&self.window, &self.map)
    }

    fn update(&mut self) {
        for (id, mut snake) in self.snakes.iter_mut().enumerate() {
            snake.update(&mut self.map, id);
        }
        use std::collections::HashMap;
        let mut heads: HashMap<(u8, u8), Vec<usize>> = HashMap::new();
        for (id, snake) in self.snakes.iter().enumerate() {
            let head = snake.body.front().expect("Snake with no body");
            let entry = heads.entry((head.x, head.y)).or_insert_with(Vec::new);
            entry.push(id);
        }
        for ids in heads.values() {
            if ids.len() > 1 {
                for id in ids {
                    self.snakes[*id as usize].is_dead = true;
                }
            }
        }
        for snake in &self.snakes {
            if snake.is_dead {
                snake.clear(&mut self.map);
            }
        }
    }
}

#[derive(Default)]
pub struct GameSettings {
    width: u8,
    height: u8,
    snakes: Vec<Snake>,
}

impl GameSettings {
    pub fn new() -> GameSettings {
        Default::default()
    }

    pub fn width(self, width: u8) -> GameSettings {
        GameSettings { width, ..self }
    }

    pub fn height(self, height: u8) -> GameSettings {
        GameSettings { height, ..self }
    }

    pub fn snake(self, snake: Snake) -> GameSettings {
        let mut snakes = self.snakes;
        snakes.push(snake);

        GameSettings { snakes, ..self }
    }

    pub fn build(self) -> Game {
        let mut map = vec![vec![Tile::Empty; self.height as usize]; self.width as usize];
        map[0] = vec![Tile::Wall; self.height as usize];
        map[self.width as usize - 1] = vec![Tile::Wall; self.height as usize];
        for i in 1..self.width - 1 {
            map[i as usize][0] = Tile::Wall;
            map[i as usize][self.height as usize - 1] = Tile::Wall;
        }

        let window = pancurses::initscr();

        let game = Game {
            width: self.width,
            height: self.height,
            snakes: self.snakes,
            window,
            renderer: Renderer::new(),
            map,
        };

        game
    }
}

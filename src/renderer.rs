extern crate pancurses;

use Tile;

use Map;

#[derive(Debug)]
pub struct Renderer {
    width: u32,
    height: u32,
    game_width: u32,
    scoreboard_height: u32,
}

impl Renderer {
    pub fn new() -> Renderer {
        Renderer {
            width: 120,
            height: 40,
            game_width: 80,
            scoreboard_height: 20,
        }
    }

    pub fn draw(&self, window: &pancurses::Window, map: &Map) {
        window.erase();

        self.draw_borders(window);
        self.draw_game(window, map);

        window.mv(0, 0);

        window.refresh();
    }

    fn draw_game(&self, window: &pancurses::Window, map: &Map) {
        let left = (self.game_width / 2) - (map.len() as u32 / 2) + 1;
        let top = (self.height / 2) - (map[0].len() as u32 / 2);
        for y in 0..map[0].len() {
            window.mv(y as i32 + top as i32, left as i32);
            for x in 0..map.len() {
                let out = match map[x as usize][y as usize] {
                    Tile::Empty => ' ',
                    Tile::Wall => 'X',
                    Tile::Snake(_) => '#',
                    Tile::Apple => '*',
                };
                window.addch(out);
            }
        }
    }

    fn draw_borders(&self, window: &pancurses::Window) {
        use pancurses as pc;
        window.attron(pc::A_REVERSE);
        self.draw_line(window, 0, 0, self.width, false);
        self.draw_line(window, 0, 0, self.height, true);
        self.draw_line(window, 0, self.height - 1, self.width, false);
        self.draw_line(window, self.width - 1, 0, self.height, true);
        self.draw_line(window, self.game_width + 1, 0, self.height, true);
        self.draw_line(
            window,
            self.game_width + 1,
            self.scoreboard_height + 1,
            self.width - self.game_width - 1,
            false,
        );
        self.draw_line(
            window,
            self.game_width + 1,
            2,
            self.width - self.game_width - 1,
            false,
        );
        window.attroff(pc::A_REVERSE);
        window.mv(1, self.game_width as i32 + 2);
        window.attron(pc::A_BOLD);
        window.printw("       MAKE SNAKE GREAT AGAIN");
        window.attroff(pc::A_BOLD);
    }

    fn draw_line(&self, window: &pancurses::Window, x: u32, y: u32, length: u32, vertical: bool) {
        window.mv(y as i32, x as i32);
        if vertical {
            window.vline(' ', length as i32);
        } else {
            window.hline(' ', length as i32);
        }
    }
}

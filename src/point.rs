use direction::Direction;

#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct Point {
    pub x: u8,
    pub y: u8,
}

impl Point {
    pub fn translate(self, dir: Direction) -> Self {
        use Direction::*;

        let mut new_point = self;

        match dir {
            Up => new_point.y -= 1,
            Down => new_point.y += 1,
            Left => new_point.x -= 1,
            Right => new_point.x += 1,
        }

        new_point
    }
}

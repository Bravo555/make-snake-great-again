use std::collections::LinkedList;

use direction::Direction;
use point::Point;
use Tile;

#[derive(Debug)]
pub struct Snake {
    pub body: LinkedList<Point>,
    pub direction: Direction,
    pub is_dead: bool,
}

impl Snake {
    pub fn new(x: u8, y: u8) -> Snake {
        let mut body: LinkedList<Point> = LinkedList::new();
        body.push_front(Point { x, y });
        Snake {
            body,
            direction: Default::default(),
            is_dead: false,
        }
    }

    pub fn update(&mut self, map: &mut Vec<Vec<Tile>>, id: usize) {
        if self.is_dead {
            return;
        };
        let new_head = self.body.front().unwrap().translate(self.direction);

        let tile = map[new_head.x as usize][new_head.y as usize];

        match tile {
            Tile::Wall | Tile::Snake(_) => self.is_dead = true,
            _ => {
                map[new_head.x as usize][new_head.y as usize] = Tile::Snake(id);
            }
        }
        self.body.push_front(new_head);

        if tile != Tile::Apple {
            let last = self.body
                .pop_back()
                .expect("Tried to pop snake of length 0");
            map[last.x as usize][last.y as usize] = Tile::Empty;
        }
    }

    pub fn clear(&self, map: &mut Vec<Vec<Tile>>) {
        for node in &self.body {
            let x = node.x as usize;
            let y = node.y as usize;

            if let Tile::Snake(_) = map[x][y] {
                map[x][y] = Tile::Empty;
            }
        }
    }
}
